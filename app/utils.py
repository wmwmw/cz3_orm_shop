from .models import Account, AccountBasket, Address, Category, Order, OrderProduct, Product
from .db import database

def create_database():
    database.create_tables(
        [
            Account,
            AccountBasket,
            Address,
            Category,
            Order,
            OrderProduct,
            Product,
            Product.categories.through_model
        ]
    )