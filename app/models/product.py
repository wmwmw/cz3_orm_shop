import peewee

from .base import Model
from .category import Category


class Product(Model):
    name = peewee.TextField()
    description = peewee.TextField(null=True)
    price = peewee.DecimalField(decimal_places=2, constraints=[peewee.Check("price > 0.0")])
    barcode = peewee.CharField(max_length=60, unique=True)
    categories = peewee.ManyToManyField(Category)
