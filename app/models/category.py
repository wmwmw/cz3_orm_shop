import peewee

from .base import Model


class Category(Model):
    name = peewee.TextField()
    description = peewee.TextField()
    parent = peewee.DeferredForeignKey('Category', deferrable='INITIALLY DEFERRED', null=True)
    short_name = peewee.CharField()

# CREATE TABLE IF NOT EXISTS "category" (
#   "id" INTEGER NOT NULL PRIMARY KEY,
#   "name" TEXT NOT NULL,
#   "description" TEXT NOT NULL,
#   "short_name" VARCHAR(255) NOT NULL,
#   "parent_id" INTEGER NOT NULL
# )
