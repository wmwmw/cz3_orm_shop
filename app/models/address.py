import peewee

from .base import Model
from .account import Account


class Address(Model):
    city = peewee.CharField()
    street = peewee.CharField()
    house_number = peewee.CharField()
    account = peewee.ForeignKeyField(Account, on_delete="CASCADE")


# CREATE TABLE IF NOT EXISTS "address" (
#   "id" INTEGER NOT NULL PRIMARY KEY,
#   "city" VARCHAR(255) NOT NULL,
#   "street" VARCHAR(255) NOT NULL,
#   "house_number" VARCHAR(255) NOT NULL,
#   "account_id" INTEGER NOT NULL,
#   FOREIGN KEY ("account_id") REFERENCES "account" ("id") ON DELETE CASCADE
# )

