import peewee

from .base import Model
from .account import Account
from .product import Product
from .address import Address

DeferredOrderProduct = peewee.DeferredThroughModel()


class Order(Model):
    account = peewee.ForeignKeyField(Account, on_delete="CASCADE")
    invoicing_address = peewee.ForeignKeyField(Address)
    delivery_address = peewee.ForeignKeyField(Address)
    products = peewee.ManyToManyField(Product, through_model=DeferredOrderProduct)

    @staticmethod
    def total_value():
        return OrderProduct.select(peewee.fn.sum(OrderProduct.real_price)).first().real_price

    @staticmethod
    def total_items():
        return OrderProduct.select().count()


class OrderProduct(Model):
    order = peewee.ForeignKeyField(Order, on_delete="CASCADE")
    product = peewee.ForeignKeyField(Product)
    serial_number = peewee.CharField()
    real_price = peewee.DecimalField(decimal_places=2)


DeferredOrderProduct.set_model(OrderProduct)
