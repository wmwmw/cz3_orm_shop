from peewee import Model as PeeweeModel

from app.db import database


class Model(PeeweeModel):
    class Meta:
        database = database
