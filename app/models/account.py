import peewee

from .base import Model
from .product import Product

DeferredAccountBasket = peewee.DeferredThroughModel()


class Account(Model):
    username = peewee.CharField(unique=True, constraints=[peewee.Check('length(username) >= 3')])
    is_individual = peewee.BooleanField(default=False)
    is_company = peewee.BooleanField(default=False)
    is_vat_payer = peewee.BooleanField(default=False)
    password = peewee.TextField()
    email = peewee.CharField()
    is_active = peewee.BooleanField(default=True)
    basket = peewee.ManyToManyField(Product, through_model=DeferredAccountBasket)

    class Meta:
        constraints = [peewee.Check("((is_individual or is_company) and not (is_individual and is_company))")]


# CREATE TABLE IF NOT EXISTS "account" (
#   "id" INTEGER NOT NULL PRIMARY KEY,
#   "username" VARCHAR(255) NOT NULL CHECK (length(username) >= 3),
#   "is_individual" INTEGER NOT NULL,
#   "is_company" INTEGER NOT NULL,
#   "is_vat_payer" INTEGER NOT NULL,
#   "password" TEXT NOT NULL,
#   "email" VARCHAR(255) NOT NULL CHECK (email REGEXP '^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+$'),
#   "is_active" INTEGER NOT NULL,
#   CHECK (((is_individual or is_company) and not (is_individual and is_company)))
# )

class AccountBasket(Model):
    product = peewee.ForeignKeyField(Product, on_delete="CASCADE")
    account = peewee.ForeignKeyField(Account, on_delete="CASCADE")
    quantity = peewee.SmallIntegerField(constraints=[peewee.Check("quantity > 0")])


DeferredAccountBasket.set_model(AccountBasket)

# create table account_product_basket(
# 	account_id bigint references account(id),
# 	product_id bigint references product(id),
# 	quantity smallint check (quantity > 0),
# 	primary key (account_id, product_id)
# );
