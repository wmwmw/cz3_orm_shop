import faker

from .models import Account, Address, AccountBasket, Product, OrderProduct, Category, Order

fake = faker.Faker()


def gen_user():
    individual = fake.boolean()
    return {
        "username": fake.unique.user_name(),
        "email": fake.email(),
        "is_individual": individual,
        "is_company": not individual,
        "password": fake.paragraph(),
    }


def gen_address(user_ids: [int]):

    return {
        "city": fake.city(),
        "street": fake.street_name(),
        "house_number": fake.building_number(),
        "account_id": fake.random_choices(user_ids, length=1)[0]
    }


def gen_categories():
    return {
        "name": fake.slug(),
        "short_name": fake.slug(),
        "description": fake.slug()
    }


def gen_product():
    return {
        "name": fake.word(),
        "price": "{:.2f}".format(fake.random_int(100, 100000) / 100),
        "description": fake.sentence(),
        "barcode": fake.uuid4()
    }


def gen_product_categories(category_ids: [int], product_ids: [int]):
    return {
        "category_id": fake.random_choices(category_ids, length=1)[0],
        "product_id": fake.random_choices(product_ids, length=1)[0]
    }


def gen_product_basket(user_ids: [int], product_ids: [int]):
    return {
        "account_id": fake.random_choices(user_ids, length=1)[0],
        "product_id": fake.random_choices(product_ids, length=1)[0],
        "quantity": 1
    }


def gen_orders(user_ids: [int], address_ids: [int]):
    return {
        "account_id": fake.random_choices(user_ids, length=1)[0],
        "invoicing_address_id": fake.random_choices(address_ids, length=1)[0],
        "delivery_address_id": fake.random_choices(address_ids, length=1)[0]
    }


def gen_product_orders(order_ids: [int], product_ids: [int]):
    return {
        "order_id": fake.random_choices(order_ids, length=1)[0],
        "product_id": fake.random_choices(product_ids, length=1)[0],
        "serial_number": fake.uuid4(),
        "real_price": "{:.2f}".format(fake.random_int(100, 100000) / 100)
    }


def generate_data():
    for _ in range(100):
        Account.create(**gen_user())

    user_ids = [a.id for a in Account.select(Account.id)]

    for _ in range(1000):
        Address.create(**gen_address(user_ids))

    address_ids = [a.id for a in Address.select(Address.id)]

    for _ in range(20):
        Category.create(**gen_categories())

    category_ids = [c.id for c in Category.select(Category.id)]

    for _ in range(2000):
        Product.create(**gen_product())

    product_ids = [p.id for p in Product.select(Product.id)]

    for _ in range(10):
        Product.categories.through_model.create(**gen_product_categories(category_ids, product_ids))

    for _ in range(2000):
        AccountBasket.create(**gen_product_basket(user_ids, product_ids))

    for _ in range(4000):
        Order.create(**gen_orders(user_ids, address_ids))

    order_ids = [o.id for o in Order.select(Order.id)]

    for _ in range(10000):
        OrderProduct.create(**gen_product_orders(order_ids, product_ids))
