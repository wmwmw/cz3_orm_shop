import sys
from app.utils import create_database
from app.data_generator import generate_data

if len(sys.argv) < 2:
    print("Nebyl zadan prikaz, moznosti jsou: createdatabase")

elif sys.argv[1] == "createdatabase":
    create_database()
    print("Databaze uspesne vytvorena.")

elif sys.argv[1] == "generatedata":
    generate_data()
    print("Databaze byla naplnena.")

else:
    print(f"Nezadan platny prikaz, zadano {sys.argv[1]}.")
